# =============================
# 1. standard pelican variables
# =============================
SITEURL      = 'http://galleria.artliba.org'
SITENAME     = 'Galleria'
AUTHOR       = 'Pirogh Hesse'
SITESUBTITLE = 'A responsive pelican gallery plugin'
TIMEZONE     = 'Europe/Paris'
DEFAULT_LANG = 'en'
LOCALE       = 'C'

PATH         = 'content'
STATIC_PATHS = ['images', 'theme']

OUTPUT_PATH  = 'output/'

DISPLAY_CATEGORIES_ON_MENU = False # choose a custom top menu here
MENUITEMS = (
  ('About',      'about.html'),
  ('All',        'index.html'),
  ('Overview',   '1a-getting-started.html'),
  ('Directive',  '1b-directive.html'),
  ('Config',     '1c-pelicanconf.html'),
  ('#Tags',      'tags.html'),
)
DEFAULT_PAGINATION =   6; # article summaries per page
SUMMARY_MAX_LENGTH = 100; # counted in words

RELATIVE_URLS                    = True
SLUGIFY_SOURCE                   = 'basename' 
ARTICLE_URL   = ARTICLE_SAVE_AS  = '{slug}.html'
CATEGORY_URL  = CATEGORY_SAVE_AS = '{slug}.html'
TAG_URL       = TAG_SAVE_AS      = '{slug}.html'
AUTHOR_URL    = AUTHOR_SAVE_AS   = '{slug}.html'
ARCHIVES_SAVE_AS                 = 'archives.html'
YEAR_ARCHIVE_SAVE_AS             = 'archive_{date:%Y}.html'
MONTH_ARCHIVE_SAVE_AS            = 'archive_{date:%Y}_{date:%m}.html'
# =============================
# 2. Lepagito theme
# =============================
THEME = '/home/hesse/dvt-perso/lepagito'
#TAGS_BY_GROUPS = (
#  ('Pelican',       ('tag-group', 'navigation', 'plugin', 'pelicanconf.py')),
#)
FOOTERMENUITEMS = (
  ('About',    'about.html'),
  ('License',  'about.html#license-1'),
)
LEPAGITO_NAVIGATION_SWAP = True
# =============================
# 3. plugins
# =============================
PLUGIN_PATHS  = ['/home/hesse/dvt-perso/theme-config/pelican/plugins',
                 '/home/hesse/dvt-perso/galleria/pelican/plugins']
PLUGINS = ['theme_config', 'neighbors', 'galleria']
GALLERIA_LIBRARY_PATH          = ['/home/hesse/dvt-perso/galleria/doc/content']
LEPAGITO_GALLERY_CSS_FILE      = 'galleria.css'
GALLERIA_PROCESS = {
    'image-icon': {
        'type': 'image',
        'ops': ['50x50', '+profile "*"'],
    },
    'image-medium': {
        'type': 'image',
        'ops': ['300x300', '+profile "*"'],
    },
    'crisp': {
        'type': 'responsive-image',
        'srcset': [
            ('1x', [ '300x225', '+profile "*"']),
            ('2x', [ '600x450', '+profile "*"']),
            ('3x', ['1200x900', '+profile "*"']),
        ],
        'default': '1x',
    },
    'responsive-medium': {
        'type': 'responsive-image',
        'sizes': (
            '(min-width: 768px) 650px, '
            '(min-width: 1200px) 800px, '
            '100vw'
        ),
        'srcset': [
            ('600w', ['600x450', '+profile "*"']),
            ('800w', ['800x600', '+profile "*"']),
        ],
        'default': '800w',
    },
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
   'picture-tag': {
       'type': 'picture',
       'sources': [
           {
               'name': 'default',
               'media': '(min-width: 640px)',
               'srcset': [
                   ( '640w', [ '640x480',  '+profile "*"']),
                   ('1024w', ['1024x683',  '+profile "*"']),
                   ('1600w', ['1600x1200', '+profile "*"']),
               ],
               'sizes': '100vw',
           },
           {
               'name': 'source-1',
               'srcset': [
                   ('1x', ['200x200']),
                   ('2x', ['300x300']),
               ]
           },
       ],
       'default': ('default', '640w'),
   },
}
GALLERIA_DEFAULT_CLASS      = 'picture-tag' # should be defined
GALLERIA_DEFAULT_MAX_HEIGHT = '600px'
GALLERIA_ICON_CLASS         = 'image-icon'  # idem
GALLERIA_PROCESS_DIR        = "process"
GALLERIA_RESIZE_JOBS        = 10
