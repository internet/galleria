.. include:: common.txt

The galleria directive
======================
:date: 2023-02-27
:category: Directive
:tags: option

.. _introduction section: 1a-getting-started.html

A "galleria" consists of list of image file names, one per line,
followed by a list of options, one per line, and then an optional
caption paragraph.
There must be blank lines before the caption and target paragraph.
Here is an example, corresponding to the illustration of the
`introduction section`_:


.. code:: rst

  .. galleria::
    tiryns.jpg
        :caption:  Mycenaean Watercolor, from a fresco in `Tiryns`_
        :target:   https://en.wikipedia.org/wiki/Tiryns
    boxers.jpg
        :caption:  Watercolor, also a fresco from `Akrotiri`_
        :target:   `Akrotiri`_
    swallows.jpg
        :caption:  Watercolor, from a fresco from `Akrotiri`_
    caryatid.jpg
        :caption:  Watercolor, the `Caryatid`_ porch at the `Acropolis`_
        :target:
    :max-height: 350px

Options
-------

.. _flex-wrap:  https://www.w3schools.com/cssref/css3_pr_flex-wrap.php
.. _column-gap: https://www.w3schools.com/CSSref/css3_pr_column-gap.php

The ``galleria`` block could contain options.

* ``class`` `name`

  The `name` argument is a class name defined in the ``GALLERIA_PROCESS``
  dictionary variable of the ``pelicanconf.py`` file.

* ``flex-wrap`` `value`

  This option manages the `flex-wrap`_ 
  property of the ``html`` flexible container
  used by the `galleria`_ directive.
  The `value` is one of ``nowrap``, ``wrap``, ``wrap-reverse``.
  With ``nowrap``, all the image are displayed on one line.
  The ``wrap`` value specifies that the flexible items will wrap if necessary:
  this is the default value for the `galleria`_ directive.
  With ``wrap-reverse``, the flexible items will wrap, if necessary,
  but lines of the container appear in reverse order.


* ``max-height`` `length`

  The maximal height of the image.
  Used to scale the image vertically.
  When unspecified, the default value is given by the 
  ``GALLERIA_DEFAULT_MAX_HEIGHT``
  variable of the ``pelicanconf.py`` file.

* ``column-gap`` `length`

  It defines the value of the `column-gap`_ 
  property of the ``html`` flexible container
  used by the `galleria`_ directive.

Caption and target paragraph
----------------------------
There must be blank lines before the caption and target paragraph.
Each line start either by ``!caption!`` or ``!target!``, followed by
the file name and a one-line argument. 
For a target, the argument is considered as a clickable link
associated to the image.
When there is no target for an image, by default the image is 
clickable and the link is the full resolution image.
Both links and caption could involve links such as ```pelican`_``,
that are solved by the parser as `pelican`_.

In the previous example, for the first image,
the target is explicit,
for the second one, it involves a solved link 
for the third one, there is no declared target,
and for the last, there is explicitly an empty target.
When there is no declared target,
the image is automatically clickable and points
to the full resolution version of the image:
observe that the cursor changed to zoom-in.
Conversely, when there is explicitly an empty target,
the image is not clickable:
observe that the cursor do not change.


