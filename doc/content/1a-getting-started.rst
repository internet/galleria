.. include:: common.txt

Overview
--------
:date: 2023-03-27
:category: Overview
:tags: introduction

`Galleria`_ is a plugin for `pelican`_,
a static site generator written in `python`_.

.. _flex:     https://www.w3schools.com/cssref/css3_pr_flex.php
.. _gimp:     https://www.gimp.org

Observe it first in action here: try to resize the window of our web
browser and observe the flexible rearrangement of the following images.



.. _archaeological museum: https://en.wikipedia.org/wiki/National_Archaeological_Museum,_Athens
.. _Tiryns:    https://en.wikipedia.org/wiki/Tiryns
.. _Akrotiri:  https://en.wikipedia.org/wiki/Akrotiri_(prehistoric_city)
.. _Caryatid:  https://en.wikipedia.org/wiki/Caryatid
.. _Acropolis:  https://en.wikipedia.org/wiki/Acropolis_of_Athens

.. galleria::
    tiryns.jpg
        :caption:  Mycenaean Watercolor, from a fresco in `Tiryns`_
        :target:   https://en.wikipedia.org/wiki/Tiryns
    boxers.jpg
        :caption:  Watercolor, also a fresco from `Akrotiri`_
        :target:   `Akrotiri`_
    swallows.jpg
        :caption:  Watercolor, from a fresco from `Akrotiri`_
    caryatid.jpg
        :caption:  Watercolor, the `Caryatid`_ porch at the `Acropolis`_
        :target: 
    :max-height: 350px


The `galleria`_ plugin implements a `reStructuredText`_
directive for defining galleries in an intuitive way.
Observe some real-life application of this plugin: just have a look at
the `Greuze-Cottave`_ web site or the present author's `Pirogh`_ site.

The previous galleria shows four watercolors from 2022 by `Pirogh`_,
which is also the  author of the present plugin.
The original models was three Mycenaean fresco from the ancient Greece done between 1400 and 1200 BC,
which could be observed in the `archaeological museum`_  in Athens, and the last is a watercolor
on the motif that represents the `Caryatid`_ porch at the `Acropolis`_.

Try to resize your window browser:
observe the flexible rearrangement of the images.
This is possible thanks to the `flex`_ container feature implemented by the 
`galleria`_ plugin.
This is especially useful when displaying on mobile phones.
Moreover, this plugin is able to automatically minimize the overall page weight:
it save you a trip to `gimp`_ each time you include an image in your post.
It also makes it easy to create responsive images using the ``html5 srcset``
attribute and ``<picture>`` tag.
It does this by automatically generating multiple derivative images
from one or more sources.
It is possible efficiently to manage web site with thousands of large images,
and each gallery could displays hundred of images: the result will optimize
the bandwidth of the connection, depending upon the resolution of the user's display.
Finally the thumbnail's image resolution itself is also responsive,
in the sense that it takes into account the physical resolution
of our window and screen resolution.
When you click on a thumbnail of a picture, a full resolution version
of this picture is displayed.
The `galleria`_ will never overwrite your original images.

You're interested? Fascinated? Let's start!
First, download and install the plugin:

.. code:: bash

  git clone https://gricad-gitlab.univ-grenoble-alpes.fr/internet/galleria

