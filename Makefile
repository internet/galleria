SUBDIRS = 		\
	pelican		\
	doc		\

DISTFILES = 		\
	Makefile 	\
	README.rst	\
	0TODO.txt	\

default: all

all install clean::
	@for d in $(SUBDIRS); do \
	  cd $$d && $(MAKE) -s $@ && cd ..; \
	  if test $$? -ne 0; then exit 1; fi; \
	done

-include $(CVSMKROOT)/git.mk
