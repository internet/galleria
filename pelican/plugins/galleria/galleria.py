#
# The Galleria pelican plugin
#
# date: 21 march 2023
# 
# Copyright (C) 2023 Pierre.Saramito@imag.fr
#
# Galleria is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Galleria is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Galleria; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# =============================================================================
# implementation notes
# =============================================================================
# the rst2hml parser is in two steps: rst2node & node2html
# * rst2node :
#   performed by the Galleria directive, that output a special "galleria" node
#   this node do not produce any text directly,
#   but store in node['data'] attribute all we need to build the galleria
#   with the jinja template
# * node2html :
#   the node2html translator is registered in the pelican environment
#
# File content   
# 0. global values
# 1. rst2node
# 2. node2html
# 3. image compression
# 4. pelican initialization
#
# =============================================================================
# 0. imports & global values
# =============================================================================

import sys
import logging
import docutils
from typing import Any, Dict, List
from pelican import Pelican, signals
from pelican.readers import PelicanHTMLTranslator, RstReader, Readers, PelicanHTMLWriter
from pelican.contents import Article, Page, Content
from pelican.generators import Generator, ArticlesGenerator, PagesGenerator

# globals:
g_logger = logging.getLogger(__name__)
g_pelican_settings: Dict[str, Any] = {}
g_process_list = []
g_generator = None
g_my_reader = None
g_my_writer = None
g_my_translator = None

# =============================================================================
# 1. rst2node
# =============================================================================
# -----------------------------------------------------------------------------
# 1.0 utilities
# -----------------------------------------------------------------------------
from docutils import nodes
from docutils.parsers.rst import directives, Directive

# remove whites at begining and end
def my_white_cleaner (x):
    import re
    y = re.sub (r'^[\s]+', '', x)
    y = re.sub (r'[\s]+$', '', y)
    return y

def indentation_check (options_string, image_filename):
    import re
    # example:
    # options_string = 
    # |    :caption:  Mycenaean Watercolor, from a fresco in `Tiryns`_
    # |    :target:   https://en.wikipedia.org/wiki/Tiryns
    # => check that the number of spaces at the beginning of the lines is the same
    #sys.stderr.write (f'indentation_check...\n')
    #sys.stderr.write (f'image_filename = "{image_filename}"\n')
    #sys.stderr.write (f'options_string = "{options_string}"\n')
    options_list = options_string.split ('\n')
    #sys.stderr.write (f'options_list = {options_list}\n')
    n_indent = -1
    for option_line in options_list:
        indent = re.sub (r'[^\s].*', '', option_line)
        #sys.stderr.write (f'indent = "{indent}"\n')
        if n_indent == -1:
            n_indent = len(indent)
        elif len(indent) != n_indent:
            option_line_clean = my_white_cleaner (option_line)
            option_line_list = option_line_clean.split (' ', 1)
            option_name = my_white_cleaner (option_line_list[0])
            if len(option_line_list) == 0: continue # empty line
            option_name = my_white_cleaner (option_line_list[0])
            raise SystemExit (f'galleria error: inconsistent indentation for option "{option_name}" of image file "{image_filename}"')
    #sys.stderr.write (f'indentation_check done\n')

# -----------------------------------------------------------------------------
# 1.1 the galleria directive is defined in the docutils environment
# -----------------------------------------------------------------------------
# declare the custom "galleria" node type:
class galleria(nodes.Structural, nodes.Element):
    pass

class Galleria(Directive):

    flex_wrap_values = ('nowrap', 'wrap', 'wrap-reverse')

    def flex_wrap(argument):
        return directives.choice (argument, Galleria.flex_wrap_values)
 
    # defines the parameter the directive expects
    # directives.unchanged means you get the raw value from RST
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {'class': directives.unchanged,
                   'flex-wrap': flex_wrap,
                   'max-height': directives.unchanged, # eg 100hv, otherwise directives.length_or_unitless,
                   'column-gap': directives.length_or_unitless,
                   }
    has_content = True # no content as figure legend after the directive

    def run(self):
        # note: 'data' & 'options' attached as attributes to the returned node
        # are transmitted to visit_galleria for the html translator,
        # where there are transmitted to the jinja template galleria.html
        import re
        #print('run0: self.arguments =', self.arguments)
        import os
        global g_pelican_settings
        galleria_node = galleria()
        image_option_dict: Dict[str, Any] = {}
        arg_full_text = self.arguments[0]
        # drop comments from '#' until '\n':
        arg_full_text = re.sub(r'#[^\n]+', '', arg_full_text)
        #sys.stderr.write (f'arg_full_text = "{arg_full_text}"\n')
        # example of arg_full_text:
        # 
        # |file1.jpg
        # |    :caption: `pelican`_ caption1
        # |    :target:  `pelican`_ link1
        # |file2.jpg
        # |    :caption: caption2
        # |file3.jpg
        # |    :caption: my caption3
        # |    :target:  target3
        #
        # difficulty: captions and links could contain some `things`_ like that 
        # that should be solved, so put it in a "caption" paragraph like that:
        #
        # |!caption! file1.jpg `pelican`_ caption1
        # |!target!  file1.jpg `pelican`_ link1
        # |!caption! file2.jpg caption2
        # |!caption! file3.jpg my caption3
        # |!target!  file3.jpg target3
        #
        # This paragraph is then parsed and links like `things`_ are solved
        # Finally, we get this solved text back in the "depart_galleria" subtoutine
        #
        # split it by \n when \n is not followed by some white space 
        p = re.compile (r'\n(?=[^ \t])', re.M)
        arg_list = p.split (arg_full_text)
        #sys.stderr.write (f'arg_list = {arg_list}\n')
        # ------------------------------------
        # 1. loop on args and build image_list
        # ------------------------------------
        library_path = g_pelican_settings['GALLERIA_LIBRARY_PATH']
        #print('run1: library_path =', library_path)
        image_list = []
        # option_paragraph_list: docutils.statemachine.StringList(initlist=[])
        option_paragraph_list = []
        option_paragraph_offset = 0
        for arg in arg_list:
            arg = my_white_cleaner (arg)
            #sys.stderr.write (f'arg = "{arg}"\n')
            if len(arg) == 0:
                # empty line: probably after comment drop
                continue
            arg_splited = arg.split (' ', 1)
            #sys.stderr.write (f'arg_splited = {arg_splited}\n')
            #sys.stderr.write (f'len(arg_splited) = {len(arg_splited)}\n')
            image_filename = my_white_cleaner (arg_splited[0])
            #sys.stderr.write (f'image_filename = "{image_filename}"\n')
            image_pattern = directives.uri (image_filename) 
            #sys.stderr.write (f'image_pattern  = "{image_pattern}"\n')
            image_path_list = find_file_in_path_by_pattern (image_pattern, library_path)
            # error when filename not found or multiple match
            if len(image_path_list) == 0:
                raise SystemExit (f'galleria error: file {image_pattern} not found in GALLERIA_LIBRARY_PATH')
            if len(image_path_list) >= 2:
                raise SystemExit (f'galleria error: multiple occurence of file {image_pattern} in GALLERIA_LIBRARY_PATH')
            #sys.stderr.write (f'image_path_list = {image_path_list}\n')
            for image_relative_path, image_full_path in image_path_list:
                #image_basename = os.path.basename (image_relative_path)
                #image_dirname = os.path.dirname (image_relative_path)
                image_list.append ([image_relative_path, image_full_path])
                image_option_dict[image_relative_path]: Dict[str, Any] = {}
                #print('image_relative_path =', image_relative_path)
            #sys.stderr.write (f'arg_splited = {arg_splited}\n')
            #sys.stderr.write (f'len(arg_splited) = {len(arg_splited)}\n')
            if len(arg_splited) == 1:
                # no option at all, just a filename
                continue
            # here: have options such as :caption: or :target:
            options_string = ' ' + arg_splited[1] # restore the white space at the beginning
            # check indentation
            indentation_check (options_string, image_filename)
            q = re.compile (r'\n', re.M)
            options_list = q.split (options_string)
            for option_line in options_list:
                option_line = my_white_cleaner (option_line)
                option_list = option_line.split (' ', 1)
                if len(option_list) == 0: continue
                option_name = my_white_cleaner (option_list[0])
                if option_name != ':caption:' and option_name != ':target:':
                    raise SystemExit (f'galleria error: unexpected option "{option_name}"')
                option_name = re.sub (':', '!', option_name)
                if len(option_list) >= 2:
                    option_arg = my_white_cleaner (option_list[1])
                else:
                    option_arg = ''
                post_option_line = f'{option_name} {image_pattern} {option_arg}'
                option_paragraph_list.append (post_option_line)
                option_paragraph_offset += len(post_option_line) + 1

        #print(f'option_paragraph_list = "{option_paragraph_list}"', file=sys.stderr)
        #print('image_list =', image_list)
        # ----------------------------------------
        # 2. loop on options and build option_dict
        # ----------------------------------------
        #print('run2: self.options =', self.options)
        option_dict: Dict[str, Any] = {}
        #print('galleria: GALLERIA_DEFAULT_CLASS =', g_pelican_settings['GALLERIA_DEFAULT_CLASS'])
        #print('galleria: GALLERIA_DEFAULT_MAX_HEIGHT =', g_pelican_settings['GALLERIA_DEFAULT_MAX_HEIGHT'])
        if len(arg_list) > 1:
            option_dict['class'] = g_pelican_settings ['GALLERIA_DEFAULT_CLASS']
        else:
            option_dict['class'] = g_pelican_settings ['GALLERIA_SINGLE_CLASS']
        option_dict['max-height'] = g_pelican_settings ['GALLERIA_DEFAULT_MAX_HEIGHT']
        for opt in self.options:
            value = self.options[opt]
            # -------------------------------------
            if opt == 'class':
            # -------------------------------------
                galleria_set_image_process (value, option_dict)
            else:
                option_dict[opt] = value
        #print('option_dict =', option_dict)
        # get 'type' of the 'class' : could be 
        all_class_list = ['image', 'responsive-image' , 'picture']
        process_dict = g_pelican_settings['GALLERIA_PROCESS']
        class_name = option_dict['class']
        class_type = process_dict [class_name]['type']
        if not class_type in all_class_list:
            raise SystemExit(f'galleria: invalid "{class_type}" type for class "{class_name} in GALLERIA_PROCESS')
        option_dict['type'] = class_type
        galleria_node['options'] = option_dict
        # --------------------------------------
        # 3. loop image_list and build data_dict
        # --------------------------------------
        #print('run3: image_list =', image_list)
        global g_process_list
        data_dict: Dict[str, Any] = {}
        for image_relative_path, image_full_path in image_list:
            # add image options: these 3 are not used...
            image_option_dict[image_relative_path]['max-height']   = option_dict['max-height']
            image_option_dict[image_relative_path]['class']        = option_dict['class']
            # add image options: 'replace' 'sizes' 'srcset'
            galleria_expand_image_process (image_relative_path, image_full_path, option_dict['class'], image_option_dict[image_relative_path])
            image_data = [image_relative_path, image_full_path, image_option_dict[image_relative_path]]
            data_dict [image_relative_path] : Dict[str, Any] = {}
            data_dict [image_relative_path]['fullpath'] = image_full_path
            data_dict [image_relative_path]['options']  = image_option_dict[image_relative_path]
            process_data = [image_relative_path, image_full_path, option_dict['class']]
            g_process_list.append (process_data)
        #print('run3: data_dict =', data_dict)
        galleria_node['data'] = data_dict
        #print('here2: galleria_node[data] =', galleria_node['data'])
        # -----------------------------------
        # 4. captions as self.content
        # -----------------------------------
        #print('run4: galleria: caption...')
        #print(f'run4: type(self.content)  = {type(self.content)}')
        #print(f'run4: self.content        = {self.content}')
        #print(f'run4: self.content_offset = {self.content_offset}')
        #print(f'run4: self.content.parent = {self.content.parent}')
        if len(option_paragraph_list) !=0:
            # replace the paragraph in self.content by option_paragraph_list
            tmp = docutils.statemachine.StringList (initlist = option_paragraph_list)
            tmp.parent          = self.content.parent
            tmp.parent_offset   = self.content.parent_offset
            self.content        = tmp
            self.content_offset = option_paragraph_offset
            #print(f'run4: type(self.content)  = {type(self.content)}')
            #print(f'run4: self.content        = {self.content}')
            #print(f'run4: self.content_offset = {self.content_offset}')
            #print(f'run4: self.content.parent = {self.content.parent}')
        if self.content:
            node = nodes.Element()          # anonymous container for parsing
            #print(f'run4: self.content parse...')
            self.state.nested_parse (self.content, self.content_offset, node)
            #print(f'run4: self.content parse done')
            #print('galleria: self.content =', self.content)
            #print('galleria: self.content_offset =', self.content_offset)
            #print('galleria: node =', node)
            first_node = node[0]
            #print('galleria: first_node =', first_node)
            #print('galleria: rest_node  =', node[1:])
            if isinstance (first_node, nodes.paragraph):
                caption = nodes.caption (first_node.rawsource, '',
                                        *first_node.children)
                caption.source = first_node.source
                caption.line = first_node.line
                galleria_node += caption
            elif not (isinstance (first_node, nodes.comment) and len(first_node) == 0):
                error = self.reporter.error(
                      'Galleria caption must be a paragraph or empty comment.',
                      nodes.literal_block(self.block_text, self.block_text),
                      line=self.lineno)
                return [galleria_node, error]
            if len(node) > 1:
                galleria_node += nodes.legend ('', *node[1:])
            #print('galleria_node =', galleria_node)
            #print('galleria: caption done')
        #print('galleria: done')
        return [galleria_node]

# the new rst directive is registered:
directives.register_directive ('galleria', Galleria)

# -----------------------------------------------------------------------------
# 1.2. detect the :image: metadata associated to an article
#      and propagates it as "galleria_image" in jinja templates such as
#         article.html index.html archives.html periodic_archives.html
#       pelican register this as handle_all_generators_finalized
# -----------------------------------------------------------------------------
def detect_meta_images (content: Content):
    # Look for article or page photos specified in the meta data
    # Find images in the generated content and replace them with the processed images
    global g_process_list
    global g_pelican_settings
    image_name = content.metadata.get ("image", None)
    if image_name:
        #print('detect_meta_images : image_name =', image_name)
        library_path = g_pelican_settings['GALLERIA_LIBRARY_PATH']
        image_path_list = find_file_in_path_by_pattern (image_name, library_path)
        if len(image_path_list) == 0:
            raise SystemExit(f'galleria: metadata "{image_name}" not found in GALLERIA_LIBRARY_PATH')
        if len(image_path_list) > 1:
            raise SystemExit(f'galleria: metadata "{image_name}" has multiple occurence in GALLERIA_LIBRARY_PATH')
        image_relative_path = image_path_list[0][0]
        image_full_path     = image_path_list[0][1]
        process_class       = g_pelican_settings ['GALLERIA_ICON_CLASS']
        #print('detect: GALLERIA_ICON_CLASS =', g_pelican_settings['GALLERIA_ICON_CLASS'])
        # action 1: make it available for jinja template as "galleria_image" and "galleria_image_process_class"
        option_dict: Dict[str, Any] = {}
        galleria_expand_image_process (image_relative_path, image_full_path, process_class, option_dict)
        content.galleria_image = option_dict['replace']
        content.galleria_image_process_class = None 
        process_data = [image_relative_path, image_full_path, process_class]
        g_process_list.append (process_data)

def handle_all_generators_finalized (generators: List[Generator]):
    #print('handle_all_generators_finalized...')
    import itertools
    for generator in generators:
        if isinstance(generator, ArticlesGenerator):
            article: Article
            article_lists: List[List[Article]] = [
                generator.articles,
                generator.translations,
                generator.drafts,
                generator.drafts_translations,
            ]
            if hasattr(generator, "hidden_articles"):
                article_lists.extend ([generator.hidden_articles, generator.hidden_translations])
            for article in itertools.chain.from_iterable (article_lists):
                detect_meta_images(article)
        elif isinstance(generator, PagesGenerator):
            page: Page
            for page in itertools.chain(
                generator.pages,
                generator.translations,
                generator.hidden_pages,
                generator.hidden_translations,
                generator.draft_pages,
                generator.draft_translations,
            ):
                detect_meta_images (page)

    #print('handle_all_generators_finalized done')

# =============================================================================
# 2. node2html
#    defines the galleria_node to html translator
#    via visit_galleria & depart_galleria
#    it calls the jinja galleria.html template with node['data']
# =============================================================================

# ----------------------------------------------------------------------------
# 2.0. handle_generator_init: just to get the "generator" variable as global
# -----------------------------------------------------------------------------

def handle_generator_init(g):
    global g_generator
    g_generator = g

# -----------------------------------------------------------------------------
# 2.1. html captions: split and store in node['data']
# -----------------------------------------------------------------------------

def captions_html_store_in_options (captions_html, data_dict):
    import re
    if len(captions_html) == 0:
        return
    global g_pelican_settings
    library_path = g_pelican_settings['GALLERIA_LIBRARY_PATH']
    #print('captions_html =', captions_html)
    captions_str = ''.join (captions_html)
    #print(f'captions_str = ||{captions_str}||')
    captions_list = re.split (r'(!caption!|!target!)', captions_str)
    # exemple : captions_list = [' ', '!caption!', 'f1.jpg légende un', '!caption!', 'f2.jpg légende deux',
    #                            '!target!', 'f2.jpg https://lien.deux', '!caption!', 'f3.jpg légende trois']"
    # remove empty strings from captions_list:
    captions_list = [x for x in captions_list if x.strip()]
    #print(f'captions_list = {captions_list}', file=sys.stderr)
    #print('data_dict =', data_dict)
    action_none    = 0
    action_caption = 1
    action_target  = 2
    action = action_none
    for arg in captions_list:
        # strip white spaces at begin and end of arg
        # ICI
        arg = re.sub (r'^[ \t\n]+', '', arg)
        arg = re.sub (r'[ \t\n]+$', '', arg)
        #print(f'arg = {arg}', file=sys.stderr)
        if action == action_none and arg == '!caption!':
            action = action_caption
            continue
        elif action == action_none and arg == '!target!':
            action = action_target
            continue
        elif action == action_none:
            raise SystemExit (f'galleria error: unexpected internal paragraph option "{arg}"')
        # here, action != action_none, we should have:  arg = '<filename> rest-of-string'
        image_restofstring_parts = arg.split(' ', 1)
        image_filename = image_restofstring_parts[0]
        if len(image_restofstring_parts) > 1:
            restofstring = my_white_cleaner (image_restofstring_parts[1])
        else:
            restofstring = ''
        #print(f'image_filename =||{image_filename}||')
        #print(f'restofstring=||{restofstring}||')
        image_path_list = find_file_in_path_by_pattern (image_filename, library_path)
        if len(image_path_list) == 0:
            raise SystemExit(f"galleria: in caption, image {image_filename} not found in GALLERIA_LIBRARY_PATH")
        elif len(image_path_list) > 1:
            raise SystemExit(f"galleria: in caption, ambiguous image {image_filename} has multiple occurences in GALLERIA_LIBRARY_PATH")
        image_relative_path = image_path_list[0][0]
        # insere dans data_dict
        if action == action_caption:
            data_dict [image_relative_path]['options']['caption'] = restofstring
        elif action == action_target:
            if len(restofstring) == 0:
                restofstring = 'none'
            elif re.findall (r'href', restofstring):
                # eg restofstring = '<a class="reference external" href="https://getpelican.com">pelican</a> link1'
                restofstring = re.sub (r'^.*href="', '', restofstring)
                restofstring = re.sub (r'">.*$',     '', restofstring)
            #print(f'target({image_filename}): restofstring=||{restofstring}||')
            data_dict [image_relative_path]['options']['target']  = restofstring
        action = action_none

# -----------------------------------------------------------------------------
# 2.2. call the galleria.html jinja template
# -----------------------------------------------------------------------------
def jinja_galleria2html (translator, data_dict, options):
    #print('galleria2html...')
    global g_pelican_settings
    relative_urls = g_pelican_settings['RELATIVE_URLS']
    if relative_urls:
        siteurl = '.'
    else:
        siteurl = g_pelican_settings['SITEURL']

    template_values = {
        'galleria': data_dict,
        'options': options,
        'SITEURL': siteurl,
    }
    template = g_generator.get_template('galleria') # i.e. galleria.html
    text = template.render(**template_values)
    #print('galleria2html done')
    return text

# -----------------------------------------------------------------------------
# 2.3. the node2html translator that recognize the new "galleria" node
# -----------------------------------------------------------------------------
g_len_html_prec: int = 0

class MyHTMLTranslator(PelicanHTMLTranslator):
    # The `HTMLTranslator` turns nodes into actual source code.
    # There is some serious magic here: when parsing nodes, `visit_[name](node)` then
    # `depart_[name](node)` are called when a node named `[name]` is encountered.
    # For details see docutils.writers.html4css1.__init__
    def __init__(self, document):
        PelicanHTMLTranslator.__init__(self, document)

    def visit_galleria(self, node):
        #print('visit-html: node[options] =', node['options'])
        #print('visit-html: node[data]    =', node['data'])
        #print('visit-html: self.body   =', self.body)
        global g_len_html_prec
        g_len_html_prec = len (self.body)

    def depart_galleria(self, node):
        #print('depart-html: self.body   =', self.body)
        # extract the text of the caption from the produced html:
        captions_html = self.body [g_len_html_prec:]
        #print('depart-html: captions_html =', captions_html)
        # and then remove it in the produced html:
        self.body = self.body [0:g_len_html_prec]
        # then process captions andstore it in options
        captions_html_store_in_options (captions_html, node['data'])
        # finally, produce the effective html output
        text = jinja_galleria2html (self, node['data'], node['options'])
        self.body.append (text)
        pass

    # html5: use the <figure> & <figcaption>
    def visit_figure(self, node):
        atts = {}
        if node.get('width'):
            atts['style'] = f"width: {node['width']}"
        if node.get('align'):
            atts['class'] = f"align-{node['align']}"
        self.body.append(self.starttag(node, 'figure', **atts))

    def depart_figure(self, node):
        if len(node) > 1:
            self.body.append('</figcaption>\n')
        self.body.append('</figure>\n')

    def visit_caption(self, node):
        if isinstance(node.parent, nodes.figure):
            self.body.append('<figcaption>')
        #self.body.append(self.starttag(node, 'p', ''))

    def depart_caption(self, node):
        #self.body.append('</p>\n')
        # <figcaption> is closed in depart_figure(), as legend may follow.
        pass

# -----------------------------------------------------------------------------
# 2.4. register into pelican this new node2html translator 
# -----------------------------------------------------------------------------
class MyHTMLWriter(PelicanHTMLWriter):
    def __init__(self):
        super().__init__()
        self.translator_class = MyHTMLTranslator

class MyRstReader(RstReader):
    # see eg RstReader in ~/mypy/lib/python3.11/site-packages/pelican/readers.py
    def __init__(self, *args, **kwargs):
        global g_my_writer
        global g_my_translator
        super().__init__(*args, **kwargs)
        self.writer_class                = MyHTMLWriter
        self.field_body_translator_class = MyHTMLTranslator
        g_my_writer = self.writer_class
        g_my_translator = self.field_body_translator_class

# pelican registers the new translator with handle_readers_init
def handle_readers_init(readers):
    # see eg the plugin 'pandoc-reader'
    global g_my_reader
    for ext in MyRstReader.file_extensions:
        readers.reader_classes[ext] = MyRstReader
        g_my_reader = readers.reader_classes[ext]

# =============================================================================
# 3. at end, process image compression used for responsive images 
#    it uses unix "gm convert" and a Makefile with parallel run
# =============================================================================
# gm convert:
# '-size NxN' gives a hint to the JPG decoder that the image is going to be downscaled to NxN
#    allowing it to  run  faster by avoiding returning full-resolution images to GraphicsMagick
#    for the subsequent resizing operation. 
# '-resize NxN' specifies the desired dimensions of the output image.
#    It will be scaled so its largest dimension is N pixels.
# ´+profile "*"' removes any  ICM,  EXIF,  IPTC,  or other profiles that might be present
#    in the input and aren't needed in the thumbnail.

def find_file_in_path_by_pattern(pattern, paths):
    import os, fnmatch, re
    from typing import List
    if not isinstance(paths, List):
      if not isinstance(paths, str):
          raise SystemExit(f"galleria: invalid path {paths}")
      paths = [paths]
    pattern_dirname  = os.path.dirname (pattern)
    pattern_basename = os.path.basename(pattern)
    result = []
    for path in paths:
        pattern_path = os.path.join (path, pattern_dirname)
        for root, dirs, files in os.walk (path):
            for name in files:
                if fnmatch.fnmatch (name, pattern_basename):
                    relative_dir = re.sub (path, '', root)
                    if relative_dir == '':
                        relative_dir = '.'
                    elif relative_dir[0] == os.sep:
                        relative_dir = re.sub (os.sep, '', relative_dir)
                    #print('walk: root = ', root, ' relative_dir =', relative_dir)
                    image_full_path = os.path.join (root, name)
                    image_relative_path = os.path.join (relative_dir, name)
                    result.append ([image_relative_path, image_full_path])
    return result

def galleria_set_image_process (value, option_dict):
    splitted_value = value.split (' ')
    if len(splitted_value) <= 0:
        raise SystemExit('galleria: "class" option should containt the process tag and optionally its type')
    option_dict['class'] = splitted_value[0]
    #print('option_dict[class]  =', option_dict['class'])

def galleria_expand_image_process (image_relative_path, image_full_path, process_class, option_dict):
    # output in option_dict
    # TODO: possible merge of code with do_process_list ?
    #print('galleria_expand_image_process...')
    import os
    global g_pelican_settings
    derivative_dir = g_pelican_settings['GALLERIA_PROCESS_DIR']
    process_dict = g_pelican_settings['GALLERIA_PROCESS']
    relative_urls = g_pelican_settings['RELATIVE_URLS']
    if relative_urls:
        siteurl = '.'
    else:
        siteurl = g_pelican_settings['SITEURL']
    specs = process_dict [process_class]
    class_type = specs['type']
    image_filename     = os.path.basename (image_relative_path)
    image_relative_dir = os.path.dirname  (image_relative_path)
    process_dir = image_relative_dir
    process_dir = os.path.join (process_dir, derivative_dir)
    process_dir = os.path.join (process_dir, process_class)
    image_replace = image_relative_path
    image_sizes = None
    image_srcset = None
    image_source_list = None
    if class_type == 'image':
         ops = specs['ops']
         wxh     = ops[0]
         options = ops[1]
         convert = [wxh, options, process_dir]
         subdir = process_dir
         image_replace = os.path.join (subdir, image_filename)
         image_sizes = None
         image_srcset = None
    elif class_type == 'responsive-image':
         #sys.stderr.write(f'BEGIN responsive-image(1)\n')
         image_replace = None
         image_sizes = ''
         if specs.get('sizes') != None and len(specs['sizes']) != 0:
             for x in specs['sizes']:
                 image_sizes = image_sizes + x
         image_srcset = ''
         default_label = specs['default']
         for label, ops in specs['srcset']:
             subdir = os.path.join (siteurl, process_dir)
             subdir = os.path.join (subdir, label)
             image_variant = os.path.join (subdir, image_filename)
             if image_srcset != '':
                 image_srcset += ', '
             image_srcset += image_variant + ' ' + label
             if label == default_label:
                 image_replace = image_variant
         if not image_replace:
             raise SystemExit(f'galleria: undefined default label {default_label} for image process class {process_class}')
         #sys.stderr.write(f'END responsive-image(1)\n')
    elif class_type == 'picture':
         default_label = specs['default'][0]
         default_size  = specs['default'][1]
         image_replace = None
         image_source_list = []
         for source_spec in specs['sources']:
             source_name   = source_spec['name']
             image_srcset = ''
             for label, ops in source_spec['srcset']:
                 subdir = os.path.join (siteurl, process_dir)
                 subdir = os.path.join (subdir, source_name)
                 subdir = os.path.join (subdir, label)
                 image_variant = os.path.join (subdir, image_filename)
                 if image_srcset != '':
                     image_srcset += ', '
                 image_srcset += image_variant + ' ' + label
                 if default_label == source_name and label == default_size:
                     image_replace = image_variant
             source_dict: Dict[str, Any] = {}
             source_dict['srcset'] = image_srcset
             if 'media' in source_spec:
                 source_dict['media'] = source_spec['media']
             if 'sizes' in source_spec:
                 source_dict['sizes'] = source_spec['sizes']
             image_source_list.append (source_dict)
         if not image_replace:
             raise SystemExit(f'galleria: undefined default label {default_label} for image process class {process_class}')
    else:
        raise SystemExit(f'galleria: unsupported image process class "{class_type}"')
    option_dict['replace'] = image_replace
    option_dict['sizes']   = image_sizes
    option_dict['srcset']  = image_srcset
    option_dict['sources'] = image_source_list
    #print ('extend: image_option_dict[',image_relative_path,'][replace] =', image_replace)
    #print('galleria_expand_image_process done')

def do_process_list():
    import sys, os, subprocess, time
    global g_pelican_settings
    global g_process_list
    verbose = g_pelican_settings['GALLERIA_VERBOSE']
    mk_name = 'galleria.mk' # TODO use /tmp and remove it
    mk = open (mk_name, 'w')
    if verbose: sys.stderr.write (f'galleria.py: file "{mk_name}" created\n')
    process_dict = g_pelican_settings['GALLERIA_PROCESS']
    output_path = os.path.relpath (g_pelican_settings['OUTPUT_PATH'])
    derivative_dir = g_pelican_settings['GALLERIA_PROCESS_DIR']
    subdir_list = []
    image_dest_list = []
    mk.write(f'all: subdir_list image_dest_list\n\n')
    for image_relative_path, image_full_path, process_class in g_process_list:
        specs = process_dict [process_class]
        class_type = specs['type']
        image_relative_dir = os.path.dirname (image_relative_path)
        process_dir = os.path.join (output_path, image_relative_dir)
        process_dir = os.path.join (process_dir, derivative_dir)
        process_dir = os.path.join (process_dir, process_class)
        convert_list = []
        if class_type == 'image':
           ops = specs['ops']
           wxh     = ops[0]
           if len(ops) > 1:
               options = ops[1]
           else:
               options = ''
           convert = [wxh, options, process_dir]
           convert_list.append (convert)
        elif class_type == 'responsive-image':
           srcset = specs['srcset']
           for label, ops in srcset:
               subdir = os.path.join (process_dir, label)
               wxh    = ops[0]
               if len(ops) > 1:
                   options = ops[1]
               else:
                   options = ''
               convert = [wxh, options, subdir]
               convert_list.append (convert)
        elif class_type == 'picture':
           for source_spec in specs['sources']:
               source_name   = source_spec['name']
               for label, ops in source_spec['srcset']:
                   subdir = os.path.join (process_dir, source_name)
                   subdir = os.path.join (subdir,      label)
                   wxh    = ops[0]
                   if len(ops) > 1:
                       options = ops[1]
                   else:
                       options = ''
                   convert = [wxh, options, subdir]
                   convert_list.append (convert)
        else:
          raise SystemExit(f'galleria: unsupported image process class "{class_type}"')
        for wxh, options, subdir in convert_list:
            image_filename = os.path.basename (image_full_path)
            image_dest = os.path.join (subdir, image_filename)
            if not image_dest in image_dest_list:
                image_dest_list.append (image_dest)
                if not subdir in subdir_list:
                    subdir_list.append (subdir)
                mk.write(f'{image_dest}: {image_full_path}\n')
                mk.write(f'\tgm convert -size {wxh} {image_full_path} -resize {wxh} {options} {image_dest}\n')
            # else: image_dest already generated: TODO could check that image_full_path is the same, for homonyms in several src dirs
    mk.write(f'\n')
    mk.write(f'subdir_list:\n')
    for subdir in subdir_list:
        mk.write(f'\tmkdir -p {subdir}\n')
    mk.write(f'\n')
    mk.write(f'IMAGE_DEST_LIST = \\\n')
    for image_dest in image_dest_list:
        mk.write(f'\t{image_dest} \\\n')
    mk.write(f'\n')
    mk.write(f'image_dest_list: $(IMAGE_DEST_LIST)\n')
    mk.write(f'\n')
    mk.write(f'clean:\n')
    mk.write(f'\trm -f $(IMAGE_DEST_LIST)\n')
    mk.write(f'\n')
    mk.close()
    nproc: int = g_pelican_settings["GALLERIA_RESIZE_JOBS"]
    if nproc < 0:
        nproc = 1
    elif nproc == 0:
        nproc = os.cpu_count()
        if nproc <= 0:
            nproc = 1
    command = f'make -j{nproc} -f {mk_name} clean all > process.log'
    if verbose: sys.stderr.write (f'! {command}\n')
    t0 = time.perf_counter()
    result = subprocess.run (command, shell=True, check=True)
    t1 = time.perf_counter()
    duration = t1 - t0
    if verbose: sys.stderr.write (f'! convert time = {duration:.2f} seconds\n')
    if result.returncode != 0:
        raise SystemExit(f'galleria: convert command failed (status={result.returncode})')

def handle_finalized(pelican: Pelican):
    #print('handle_finalized...')
    do_process_list()
    #print('handle_finalized done')

# =============================================================================
# 4. pelican initialization & signal registration
# =============================================================================

# galleria plugin auto-configuration:
def plugin_config (pelican: Pelican):
    import os, sys, subprocess
    from pelican.plugins._utils import get_plugin_name, load_plugins

    #sys.stderr.write (f'galleria.py: plugin_config...\n')
    verbose = g_pelican_settings['GALLERIA_VERBOSE']
    # 1) find <path> where the 'galleria' plugin is stored
    for plugin in load_plugins (pelican.settings):
        name = get_plugin_name (plugin)
        # name = 'pelican.plugins.search' -> 'search'
        name_split = name.split ('.')
        name = name_split [len(name_split)-1]
        path_list = plugin.__path__
        if name != 'galleria': continue
        if len(path_list) == 0:
            raise SystemExit (f'galleria error: unexpected empty path for plugin {name}')
        path = path_list[0]
        #sys.stderr.write (f'galleria/plugin_config: path = {path}\n')

    # 2) add <path>/galleria/template/*  into THEME_TEMPLATES_OVERRIDES list
    #    so galleria.html will be founded
    template_dir = os.path.join (path, 'template')
    THEME_TEMPLATES_OVERRIDES = g_pelican_settings ['THEME_TEMPLATES_OVERRIDES']
    #sys.stderr.write (f'galleria/plugin_config: template_dir = {template_dir}\n')
    #sys.stderr.write (f'galleria/plugin_config: THEME_TEMPLATES_OVERRIDES = {THEME_TEMPLATES_OVERRIDES}\n')
    if not template_dir in THEME_TEMPLATES_OVERRIDES:
        if verbose: sys.stderr.write (f'galleria.py: path {template_dir} added to THEME_TEMPLATES_OVERRIDES\n')
        THEME_TEMPLATES_OVERRIDES.append (template_dir)
        g_pelican_settings ['THEME_TEMPLATES_OVERRIDES'] = THEME_TEMPLATES_OVERRIDES

    # 3) copy files <path>/galleria/theme/css/* to '{OUPUT_PATH}/{THEME_STATIC_DIR}/css/
    galleria_css = 'galleria.css'
    galleria_css_path = os.path.join (path, f'static/css/{galleria_css}')
    output_path = os.path.relpath (g_pelican_settings['OUTPUT_PATH'])
    output_css_path = os.path.join (output_path, 'theme')
    output_css_path = os.path.join (output_css_path, 'css')
    os.makedirs (output_css_path, exist_ok=True)
    output_galleria_css_path = os.path.join (output_css_path, galleria_css)
    command = f'cp {galleria_css_path} {output_galleria_css_path}'
    #if verbose: sys.stderr.write (f'! {command}\n')
    result = subprocess.run (command, shell=True, check=True)
    if result.returncode != 0:
        raise SystemExit(f'galleria/plugin_config: command failed (status={result.returncode})')
    if verbose: sys.stderr.write (f'galleria.py: file {output_galleria_css_path} imported\n')

    #sys.stderr.write (f'galleria.py: plugin_config done\n')

def handle_initialized (pelican: Pelican):
    # see eg the plugin 'photos'
    import os, sys
    #sys.stderr.write (f'galleria.py: handle_initialized...\n')
    if pelican:
        p = os.path.expanduser('.')
        #print(f'GALLERIA_LIBRARY_PATH...p={p}')
        pelican.settings.setdefault('GALLERIA_LIBRARY_PATH', p)
        pelican.settings.setdefault('GALLERIA_DEFAULT_CLASS', None)
        pelican.settings.setdefault('GALLERIA_SINGLE_CLASS', None)
        pelican.settings.setdefault('GALLERIA_ICON_CLASS', None)
        pelican.settings.setdefault('GALLERIA_DEFAULT_MAX_HEIGHT', '480px')
        pelican.settings.setdefault('GALLERIA_PROCESS_DIR', 'process')
        pelican.settings.setdefault('GALLERIA_RESIZE_JOBS', 1)
        pelican.settings.setdefault('GALLERIA_VERBOSE', True)
 
    global g_pelican_settings
    g_pelican_settings = pelican.settings
    #print('GALLERIA_DEFAULT_CLASS =', g_pelican_settings['GALLERIA_DEFAULT_CLASS'])
    #print('GALLERIA_ICON_CLASS =', g_pelican_settings['GALLERIA_ICON_CLASS'])
    #print('GALLERIA_DEFAULT_MAX_HEIGHT =', g_pelican_settings['GALLERIA_DEFAULT_MAX_HEIGHT'])
    #print(f'GALLERIA_LIBRARY_PATH=',g_pelican_settings['GALLERIA_LIBRARY_PATH'])
    plugin_config (pelican)
    #sys.stderr.write (f'galleria.py: handle_initialized done\n')

# -----------------------------------------------------------------------------
# plugin: register all handlers
# -----------------------------------------------------------------------------
def register():
    signals.initialized.connect(handle_initialized)
    signals.readers_init.connect(handle_readers_init)
    signals.generator_init.connect(handle_generator_init)
    signals.all_generators_finalized.connect(handle_all_generators_finalized)
    signals.finalized.connect(handle_finalized)
    # here: the photos code:
    #signals.content_object_init.connect(handle_content_object_init)


